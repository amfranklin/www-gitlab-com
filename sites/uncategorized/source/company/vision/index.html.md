---
layout: markdown_page
title: "GitLab Vision"
description: "GitLab is a single application based on convention over configuration  that everyone should be able to afford and adapt. With GitLab, everyone can
contribute."
canonical_path: "/company/vision/"
---

## On this page
{:.no_toc}

- TOC
{:toc}

## Vision

Our vision captures what we want the GitLab product to evolve into in the next 10 years.

Today we have a [DevSecOps Platform](/platform/). This DevSecOps Platform shift is part of a larger trend: teams of all types are realizing that breaking down silos has compounding effects on productivity and collaboration. We see it with data and operations teams creating DataOps; we see it with machine learning and ops teams creating MLOps. As more companies – and more teams within a company – rely on our platform, we are positioned to become the **AllOps platform** – a single application for all innovation. This platform will include:

1. [DevSecOps](/direction/#devsecops-stages) is our main focus in the near future since people use us as a [DevSecOps Platform](/platform/) today.
1. [ModelOps](/direction/modelops/), bringing native DataOps + MLOps into GitLab, currently DataRobot and Weights &amp; Biases are best-in-class solutions, we have begun iterating on this with an [MLFlow integration](https://docs.gitlab.com/ee/user/project/integrations/mlflow_client.html) and a Model Registry.
1. [GitLab ServiceDesk](https://docs.gitlab.com/ee/user/project/service_desk.html) which is needed to have a complete Value Stream Delivery overview. It competes with [ZenDesk](https://www.zendesk.com/) and [JIRA Service Desk](https://www.atlassian.com/software/jira/service-management/features/service-desk)

For the next few years the majority of our focus is DevSecOps as detailed in our [strategy](/company/strategy/).

## Analysts

Gartner calls the combination of DevOps/MLOps/DataOps/AIOps [xOps](https://www.gartner.com/en/documents/4001009).

## Cadence

Our [Mission](/company/mission/){:data-ga-name="mission"}{:data-ga-location="body"} is on a 30 year cadence and this in the inspiration for the vision here which is on a 10 year [cadence](/company/cadence/#vision){:data-ga-name="vision"}{:data-ga-location="body"}.

The vision inspiration for the [strategy](/company/strategy/) which is on a 3 year cadence. Additional product vision details can also be found on our [direction page](/direction/#vision){:data-ga-name="direction"}{:data-ga-location="body"}.

## Monitoring an evolving market

We'll also need to adapt with a changing market so that we meet customer needs. Netflix is a great example of this. Everyone knew that video on demand was the future. Netflix, however, started shipping DVDs over mail. They knew that it would get them a database of content that people would want to watch on demand. Timing is everything.

Additionally, we need to ensure that our Platform is open. If a new, better version control technology enters the market, we will need to integrate it into our platform, as it is one component in an integrated DevOps product.

### Entering new markets

[GitLab](https://about.gitlab.com){:data-ga-name="gitlab"}{:data-ga-location="body"} has taken existing, fragmented software tooling markets, and by offering a consolidated offering instead, have created a new [blue ocean](https://www.blueoceanstrategy.com/what-is-blue-ocean-strategy/).

We would like to find more markets where we can repeat the same model.

The desirable characteristics of such markets fall into two stages: category consolidation and creation.  They are:

#### Category Consolidation

1. A set of customer needs that are currently served by multiple, independent software tools
1. Those tools may already integrate with each other or have the possibility of integration
1. Those tools operate in categories that are typically considered discreetly (e.g. with GitLab, SCM was one market, CI another)
1. There is no current 'winner' at consolidating this market, even if there are some attempts to combine some of the tool categories within said market
1. The users of the product would also be able to contribute to it e.g. with GitLab, the users are software developers, who can directly contribute code back to the product itself
1. When initially combined the categories form a consistent and desirable user flow which solves an overriding customer requirement
1. We can offer a consolidated toolchain more cost effectively for customers, than needing to purchase, run and integrate each tool separately
1. We can do so profitably for the company

#### Category Creation

1. By combining these categories together, a new overriding category, or market, gets created - the consolidated toolchain;
1. Further adjacent categories and/or markets can be added to deliver additional user value.  For example with GitLab, you could argue that the [Protect Category](/stages-devops-lifecycle/govern/){:data-ga-name="protect"}{:data-ga-location="body"} (as of October 2019) is an adjacent category/market to be added;
1. The sum of the categories combined should have desirable [CAGR](https://investinganswers.com/dictionary/c/compound-annual-growth-rate-cagr){:data-ga-name="CAGR"}{:data-ga-location="body"} such that entering the markets will mean entering those on a growth curve;
1. Not all of the individual categories need to be on the upward curve of the [technology adoption lifecycle](https://medium.com/@shivayogiks/what-is-technology-adoption-life-cycle-and-chasm-e07084e7991f) (TAL) - it is however necessary that there are enough future categories with high CAGR and early on in their adoption lifecycle - see example (very rough) diagram outlining this below:
(insert overlapping TALs);
1. The ideal overlap of the TALs is that the peaks of the curves are as close to each other as possible, so that when one TAL moves to Late Majority, the next TAL is still curving upwards.  This allows it to provide an organic growth opportunity for the company in the markets it is entering.

Our goal is to develop this model to be more quantifiable and formulaic, so that we can quickly and easily assess new opportunities.

### Mitigating Concerns

We acknowledge the concerns to achieving our goals. We document them in our [Mitigating Concerns page](/handbook/leadership/mitigating-concerns/){:data-ga-name="mitigating concerns"}{:data-ga-location="body"}.
