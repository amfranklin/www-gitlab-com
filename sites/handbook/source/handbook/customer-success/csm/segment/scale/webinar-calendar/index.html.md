---
layout: handbook-page-toc
title: "CSM/CSE Webinar Calendar"
---
# On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [CSM Handbook homepage](/handbook/customer-success/csm/) for additional CSM/CSE-related handbook pages.

Watch previously recorded webinars on our [YouTube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kpczt4pRtyF147Uvn2bGGvq).

---

# Upcoming Webinars

We’d like to invite you to our free upcoming webinars in the months of March and April.

If you know someone who might be interested in attending, feel free to share the registration links with them. Everyone is welcome, and we hope to see you there!

## March 2023

#### Holistic Approach to Securing the Development Lifecycle
##### March 22nd, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

Join this one-hour webinar to gain a better understanding of how to successfully shift security left to find and fix security flaws during development, and to do so more easily and with greater visibility and control than typical approaches can provide.

In this session, we will go over the security and compliance features that are available with your GitLab Ultimate account, including:
- How to achieve comprehensive security scanning without introducing a multitude of tools and systems that expand your attack surface.
- How to secure and protect your cloud-native applications and IaC environments within existing DevOps workflows.
- How to use a single source of truth to improve collaboration between development and security.
- How to manage all of your software vulnerabilities in one place.
- How to set up compliance pipelines to provide consistent guardrails for developers, and ensure separation of duties

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_7aUVxkpsT2a-zdwkMa233g)

#### DevSecOps/Compliance
##### March 28th, 2023 at 12:00PM-1:00PM Eastern Time/5:00-6:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_5fj01ePBSUecCuK4MD2AvQ)

## April 2023


### AMER Time Zone Webinars

#### Git Basics
##### April 4th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Are you new to Git? Join this webinar targeted at beginners working with source code, where we will review the basics of using Git for version control and how it works with GitLab to help you get started quickly.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_YIbpX-mgRNGD73mcGIVmUA)

#### Intro to GitLab
##### April 11th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_qkqgEadIQUG6Ln4Ewl_h1g)

#### Intro to CI/CD
##### April 13th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_IYYDh_3rQ9-6vtba0WCwrA)

#### Advanced CI/CD
##### April 18th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_nq9ZMlt1Rv2YsgY-b-YYZw)

#### DevSecOps/Compliance
##### April 25th, 2023 at 12:00PM-1:00PM Eastern Time/4:00-5:00 PM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__ixfz3-5TH-JrA_XNE3ICQ)


### EMEA Time Zone Webinars

#### Intro to GitLab
##### April 4th, 2023 at 10:00AM-11:00AM UTC

Are you new to GitLab? Join this webinar, where we will review what GitLab is, how it benefits you, and the recommended workflow to allow you to get the most out of the platform.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_YWk6dJuGTwytkfKdRLFKeA)

#### Intro to CI/CD
##### April 12th, 2023 at 10:00AM-11:00AM UTC

Come learn about what CI/CD is and how it can benefit your team. We will cover an overview of CI/CD and what it looks like in GitLab. We will also cover how to get started with your first CI/CD pipeline in GitLab and the basics of GitLab Runners.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_ZZf6wP8gSN-xoV4SD2cAVQ)

#### Advanced CI/CD
##### April 18th, 2023 at 10:00AM-11:00AM UTC

Expand your CI/CD knowledge while we cover advanced topics that will accelerate your efficiency using GitLab, such as pipelines, variables, rules, artifacts, and more. This session is intended for those who have used CI/CD in the past.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN_8zFCh_7kTgSkb0NFqKnGJw)

#### DevSecOps/Compliance
##### April 25th, 2023 at 10:00AM-11:00AM UTC

GitLab enables developers and security to work together in a single tool, allowing for proactive security or “shifting left”. This session will cover what GitLab offers, how scan results integrate seamlessly with merge requests, and how to use the Security Dashboard to manage vulnerabilities.

[<button class="btn btn-primary" type="button">Register</button>](https://gitlab.zoom.us/webinar/register/WN__kkgr85eSXiw523C2NSczg)

Check back later for more webinars! 
