---
layout: handbook-page-toc
title: "Reliability:Foundations Team"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

## Mission

The mission of the Reliability:Foundations team at GitLab is to Build, Run and Own the entire lifecycle of the core infrastructure for GitLab.com.

The team is focused on owning the reliability, scalability, and security of the existing core infrastructure. We seek to reduce the effort required to provide our core infrastructure services, and to enable other teams to self-serve core infrastructure that allows them to more efficiently/effectively run their services for GitLab.com.

In order to enable Infrastructure, Development & Product teams to build their services for GitLab.com and fulfill their respective missions, we work to make the consumption of our services as simple as possible.

We seek to build our services on top of GitLab features, and use cloud vendor managed products to reduce complexity, improve efficiency and deliver new capabilities more quickly, where they are the right choice.

While the team does not explicitly have any product responsibilities, we endeavor to contribute the lessons we learn from running at-scale production systems back to the product teams, and advocate for GitLab to contain features that would allow us to DogFood.

## Vision

The Foundations Team supports the rest of Infrastructure and Development by providing the resources that other teams build upon. We do so by working collaboratively, iteratively, and striking the right balance between delivering results quickly yet safely.

## Ownership

### Services

The Services that the Foundations team is responsible for fall into two general categories: Core and Edge.

#### Core

| Service | Description | Co-Ownership? |
| ------- | ----------- | --------- |
| K8s | K8S workloads deployments, Cluster addons | Autodeploy remains with Delivery, and anything Delivery related is co-owned with Delivery |
| Config | Terraform, Chef, Image Builds | The core TF repos are owned by Foundations, while specific modules may be maintained by the teams that use them |
| Service discovery | Consul | |
| Secrets Management | Vault | Vault is offered as a service to enable teams to manage their own secrets |
| Ops | Ops.gitlab.net, Ops Runners | |

#### Edge

| Service | Description | Co-Ownership? |
| ------- | ----------- | --------- |
| CDN | Cloudflare, Fastly | |
| DNS | AWS Route 53, Cloudflare | |
| Load Balancing | HAProxy, Ingress | |
| Networking | HAProxy, Cloudflare | |
| RBAC/IAM | Teleport, GCP IAM permissions and project creation | |


### Overlap

Given the nature of this team's scope, several services the Foundation team works with are either co-owned by other teams or directly impact their work, as noted above.

## Performance Indicators

 * OKRs - OKRs are generated each quarter based on current commitments while also including spare capacity for unplanned work.
   * Success Criteria: 85% of OKRs achieved
 * Issue Metrics
   * Corrective Actions Over Time (specific to the Foundations Team)
     * Success Criteria: Must meet or exceed the [Reliability SLO](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/issues.html#service-level-agreements)
   * Lead Time
     * Success Criteria: Meets or exceeds current [Reliability SLO](https://about.gitlab.com/handbook/engineering/infrastructure/team/reliability/issues.html#service-level-agreements)
 * Customer Satisfaction
    * Success Criteria: TBD

## Team Members

<%= direct_team(manager_slug: 'amoter')%>

## Key Technical Skills

The Foundations Team must maintain a broad and diverse set of technical skills while also maintaining the ability to switch contexts frequently.  Some of these technical skills include:

 * Cloudnative Engineering - Proficiency in Kubernetes and the associated ecosystem of running cloudnative services.

 * Infrastructure as Code - Proficiency in Chef and Terraform

 * Network Systems - Understanding of network concepts and experience with our Edge stack (see Edge services above)

## Common Links

 * Slack Channel: [#g_infra_foundations](https://gitlab.slack.com/archives/C0313V3L5T6)
 * [Issue Board](https://gitlab.com/groups/gitlab-com/gl-infra/-/boards/5309050?label_name%5B%5D=team%3A%3AFoundations)
 * [Foundations team meeting agenda](https://docs.google.com/document/d/1T5LIBt3RZR5TBLzkmRd08oMwfwiNFAr5ImPD5NP7lOw/edit?usp=sharing)
 * [Foundations OKRs](https://gitlab.com/gitlab-com/gitlab-OKRs/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=Sub-Department%3A%3AReliability&label_name%5B%5D=Reliability%3A%3AFoundations&first_page_size=20)

## How We Work

 ### Values

 In addition to striving to embrace GitLab's values, the Foundations team seeks to embody the following:

  * Excellence - We do work at a high technical standard, and build maintainable systems with a long term perspective.
  * Connection - We work in a way that is collaborative and feels connected to each other.
  * Fun! - While the work we do is important, we can laugh, make jokes and enjoy our work day to day.
  * Trust and Safety - We build trust by doing what we say we’re going to do when we say we’ll do it, or communicating clearly when and why that didn’t happen. We feel safe to show up and say what is on our minds. We feel safe asking for help.
  * Ownership - We are responsible for the services we own, and we can make considered decisions about the things we are responsible for, while caring about the stakeholders who consume our work.
  * Courage - We are willing to try new things in service of improving our infrastructure. We accept the associated risks and take responsibility when things go wrong.

### Processes

* Sync meetings
  * We have weekly synchronous meetings in two different time zones to encourage discussion, give status updates, and connect as a team.
    * [Agenda](https://docs.google.com/document/d/1T5LIBt3RZR5TBLzkmRd08oMwfwiNFAr5ImPD5NP7lOw/edit?usp=sharing)
    * The meetings are recorded
  * We have monthly retrospective meetings in two different time zones
    * We review our wins and look for ways we can improve
    * These meetings are not recorded to create a safe space for sharing transparently
  * We have twice weekly Triage meetings available to us to ensure that incoming external requests are seen and responded to quickly.
    * Incoming issues are scoped and prioritized in the Triage meetings
    * They are then brought to team syncs for discussion and assignment, unless they are very urgent, in which case we will bring them to the team via Slack.

* Standup
  * We have Geekbot automated checkins on Mondays and Fridays in the [#infra_foundations_lounge](https://gitlab.slack.com/archives/C04QVEXBVL3) channel

* Communication
  * [GitLab OKRs](/handbook/engineering/okrs/) capture our commitments for each quarter and are generally updated every Tuesday
  * GitLab Epics capture large pieces of work and those labeled "In Progress" are also generally updated on Tuesdays
  * GitLab Issues capture smaller, concrete pieces of work, and those labeled "In Progress" should be updated twice weekly or whenever a portion of work has been completed.
  * Slack is the default method of reaching out between team members
    * [#g_infra_foundations](https://gitlab.slack.com/archives/C0313V3L5T6) is for work related discussions, external requests, etc
    * [#infra_foundations_lounge](https://gitlab.slack.com/archives/C04QVEXBVL3) is for socializing and standups
    * [#g_infra_foundations_notifications](https://gitlab.slack.com/archives/C04RZC5TPPD) is for automated MR notifications
 